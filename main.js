var uiData = [{
	"Phase": "Pressure80",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Efficienza5",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Pressure120",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Assessmento2",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Autostrada",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Pressure40",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Efficienza3",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Pressure160",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Assessmento1",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Efficienza4",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "FADE2",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Pressure500",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Efficienza1",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Pressure100",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "FADE1",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Pressure180-200",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Efficienza2",
	"Features": "X0",
	"Display_value": "Numeric",
	"Original_value": "X0"
},
{
	"Phase": "Pressure80",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Pressure120",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Characteristic Check",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Efficienza1",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "FADE2",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Assessmento1",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Efficienza3",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Pressure40",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Efficienza6",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},{
	"Phase": "FADE1",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Efficienza5",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Salita500",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Assessmento2",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "My Green",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Pressure100",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Pressure160",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Pressure180-200",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Efficienza2",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Efficienza4",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Autostrada",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "Pressure500",
	"Features": "X1",
	"Display_value": "Numeric",
	"Original_value": "X1"
},
{
	"Phase": "FADE2",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Efficienza3",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Autostrada",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "My Green",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Pressure500",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Pressure120",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Efficienza5",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Pressure160",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Pressure180-200",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Pressure80",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "FADE1",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Efficienza6",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Pressure100",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Assessmento1",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Efficienza4",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Efficienza2",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Assessmento2",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Characteristic Check",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Pressure40",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Efficienza1",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Salita500",
	"Features": "X2",
	"Display_value": "Numeric",
	"Original_value": "X2"
},
{
	"Phase": "Efficienza5",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "FADE1",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Pressure160",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Autostrada",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Pressure100",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Efficienza1",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Efficienza4",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Characteristic Check",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Efficienza6",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Pressure500",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Pressure40",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Pressure80",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Salita500",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Pressure180-200",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Efficienza2",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Efficienza3",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "My Green",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "FADE2",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Assessmento2",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Assessmento1",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Pressure120",
	"Features": "X3",
	"Display_value": "Numeric",
	"Original_value": "X3"
},
{
	"Phase": "Assessmento1",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Pressure160",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Pressure80",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "FADE1",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Efficienza5",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Autostrada",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Efficienza4",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Characteristic Check",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Pressure100",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Pressure40",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Assessmento2",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Efficienza2",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Salita500",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Efficienza3",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "My Green",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Pressure500",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Pressure180-200",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Efficienza6",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "FADE2",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Efficienza1",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Pressure120",
	"Features": "X4",
	"Display_value": "Numeric",
	"Original_value": "X4"
},
{
	"Phase": "Efficienza3",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Pressure80",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Assessmento1",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Characteristic Check",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Efficienza5",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Efficienza4",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Pressure100",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Pressure500",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Pressure160",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Pressure180-200",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "FADE1",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "FADE2",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "My Green",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Autostrada",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Pressure120",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Assessmento2",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Salita500",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Efficienza6",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Pressure40",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Efficienza2",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "Efficienza1",
	"Features": "X5",
	"Display_value": "Numeric",
	"Original_value": "X5"
},
{
	"Phase": "FADE1",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Efficienza4",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Autostrada",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Efficienza6",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "FADE2",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Pressure40",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Pressure100",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Efficienza1",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Pressure160",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "My Green",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Pressure180-200",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Assessmento2",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Assessmento1",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Pressure500",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Pressure120",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Efficienza3",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Pressure80",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Salita500",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Characteristic Check",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Efficienza5",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "Efficienza2",
	"Features": "X6",
	"Display_value": "Numeric",
	"Original_value": "X6"
},
{
	"Phase": "My Green",
	"Features": "X7",
	"Display_value": "Numeric",
	"Original_value": "X7"
},
{
	"Phase": "Pressure100",
	"Features": "X7",
	"Display_value": "Numeric",
	"Original_value": "X7"
},
{
	"Phase": "Assessmento2",
	"Features": "X7",
	"Display_value": "Numeric",
	"Original_value": "X7"
},
{
	"Phase": "Characteristic Check",
	"Features": "X7",
	"Display_value": "Numeric",
	"Original_value": "X7"
},
{
	"Phase": "Assessmento2",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Efficienza3",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Pressure120",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Pressure180-200",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Characteristic Check",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Efficienza1",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Assessmento1",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "FADE2",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Salita500",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Autostrada",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Efficienza6",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Pressure100",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Pressure160",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Efficienza4",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Pressure80",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Pressure40",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Pressure500",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Efficienza2",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Efficienza5",
	"Features": "PC1",
	"Display_value": "Numeric",
	"Original_value": "PC1"
},
{
	"Phase": "Assessmento1",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "My Green",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Autostrada",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Efficienza3",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Pressure500",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Efficienza5",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "FADE1",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Pressure40",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Pressure120",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Pressure160",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Characteristic Check",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Salita500",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Pressure100",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "FADE2",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Pressure180-200",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Efficienza1",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Pressure80",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Efficienza6",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Efficienza4",
	"Features": "PC2",
	"Display_value": "Numeric",
	"Original_value": "PC2"
},
{
	"Phase": "Characteristic Check",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "FADE2",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Autostrada",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Assessmento1",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Efficienza2",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Pressure180-200",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Efficienza5",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Pressure100",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Assessmento2",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Pressure160",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Efficienza1",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Pressure40",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Pressure500",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Efficienza6",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Efficienza4",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Efficienza3",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Pressure80",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Salita500",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Pressure120",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "FADE1",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "My Green",
	"Features": "PC3",
	"Display_value": "Numeric",
	"Original_value": "PC3"
},
{
	"Phase": "Pressure80",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Efficienza6",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Pressure160",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Efficienza5",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "FADE2",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Pressure120",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Assessmento2",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Pressure500",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Pressure100",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Pressure180-200",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Efficienza4",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Salita500",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "My Green",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Characteristic Check",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Pressure40",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Autostrada",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Efficienza2",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Efficienza3",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Efficienza1",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "Assessmento1",
	"Features": "PC12",
	"Display_value": "Numeric",
	"Original_value": "PC12"
},
{
	"Phase": "FADE1",
	"Features": "PS_1",
	"Display_value": "Numeric",
	"Original_value": "PS_1"
},
{
	"Phase": "FADE2",
	"Features": "PS_1",
	"Display_value": "Binary",
	"Original_value": "PS_1"
},
{
	"Phase": "Pressure40",
	"Features": "PS_1",
	"Display_value": "Binary",
	"Original_value": "PS_1"
},
{
	"Phase": "Efficienza1",
	"Features": "PS_1",
	"Display_value": "Binary",
	"Original_value": "PS_1"
},
{
	"Phase": "Assessmento1",
	"Features": "PS_2",
	"Display_value": "Binary",
	"Original_value": "PS_2"
},
{
	"Phase": "Efficienza6",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Pressure500",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Pressure100",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Autostrada",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Efficienza5",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Assessmento2",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Efficienza2",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Pressure120",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Efficienza4",
	"Features": "PS_9",
	"Display_value": "Numeric",
	"Original_value": "PS_9"
},
{
	"Phase": "Pressure80",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Efficienza1",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Pressure160",
	"Features": "PS_9",
	"Display_value": "Binary",
	"Original_value": "PS_9"
},
{
	"Phase": "Characteristic Check",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Pressure100",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Efficienza2",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Pressure120",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Pressure80",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Pressure180-200",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Efficienza5",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Pressure160",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Autostrada",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Assessmento2",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Efficienza3",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Efficienza6",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Efficienza1",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Pressure500",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "My Green",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Pressure40",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "FADE1",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "FADE2",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Efficienza4",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Assessmento1",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Salita500",
	"Features": "yeld",
	"Display_value": "Numeric",
	"Original_value": "yeld"
},
{
	"Phase": "Pressure40",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Salita500",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Pressure160",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Pressure120",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Efficienza1",
	"Features": "PS_10",
	"Display_value": "Numeric",
	"Original_value": "PS_10"
},
{
	"Phase": "Pressure80",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Pressure100",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Characteristic Check",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Pressure500",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "FADE2",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Efficienza3",
	"Features": "PS_10",
	"Display_value": "Numeric",
	"Original_value": "PS_10"
},
{
	"Phase": "Autostrada",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Efficienza5",
	"Features": "PS_10",
	"Display_value": "Binary",
	"Original_value": "PS_10"
},
{
	"Phase": "Efficienza6",
	"Features": "PS_14",
	"Display_value": "Numeric",
	"Original_value": "PS_14"
},
{
	"Phase": "Pressure160",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Assessmento2",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Efficienza6",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Pressure40",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Pressure120",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Pressure500",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Pressure100",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "FADE2",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Characteristic Check",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "My Green",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Efficienza4",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "FADE1",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Efficienza1",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Pressure80",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Autostrada",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Efficienza2",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Assessmento1",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Efficienza3",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Efficienza5",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Salita500",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Pressure180-200",
	"Features": "Pressure",
	"Display_value": "Numeric",
	"Original_value": "Pressure"
},
{
	"Phase": "Characteristic Check",
	"Features": "Oven Type",
	"Display_value": "None"
},
{
	"Phase": "Pressure80",
	"Features": "Oven Type",
	"Display_value": "None"
},
{
	"Phase": "Pressure160",
	"Features": "Oven Type",
	"Display_value": "None"
},
{
	"Phase": "Efficienza6",
	"Features": "Oven Type",
	"Display_value": "None"
},
{
	"Phase": "Assessmento1",
	"Features": "Oven Type",
	"Display_value": "None"
},
{
	"Phase": "Efficienza5",
	"Features": "Oven Type",
	"Display_value": "None"
},
{
	"Phase": "Pressure40",
	"Features": "Oven Type",
	"Display_value": "None"
},
{
	"Phase": "Salita500",
	"Features": "Oven Type",
	"Display_value": "None"
},
{
	"Phase": "Pressure80",
	"Features": "Oven Type",
	"Display_value": "IR",
	"Original_value": "oven_type_IR"
},
{
	"Phase": "Efficienza5",
	"Features": "Oven Type",
	"Display_value": "IR",
	"Original_value": "oven_type_IR"
},
{
	"Phase": "Pressure40",
	"Features": "Oven Type",
	"Display_value": "IR",
	"Original_value": "oven_type_IR"
},
{
	"Phase": "Characteristic Check",
	"Features": "Oven Type",
	"Display_value": "IR",
	"Original_value": "oven_type_IR"
},
{
	"Phase": "Pressure160",
	"Features": "Oven Type",
	"Display_value": "IR",
	"Original_value": "oven_type_IR"
},
{
	"Phase": "Efficienza6",
	"Features": "Oven Type",
	"Display_value": "IR",
	"Original_value": "oven_type_IR"
},
{
	"Phase": "Assessmento1",
	"Features": "Oven Type",
	"Display_value": "IR",
	"Original_value": "oven_type_IR"
},
{
	"Phase": "Efficienza6",
	"Features": "Oven Type",
	"Display_value": "STATICO",
	"Original_value": "oven_type_STATICO"
},
{
	"Phase": "Pressure40",
	"Features": "Oven Type",
	"Display_value": "STATICO",
	"Original_value": "oven_type_STATICO"
},
{
	"Phase": "Characteristic Check",
	"Features": "Oven Type",
	"Display_value": "STATICO",
	"Original_value": "oven_type_STATICO"
},
{
	"Phase": "Salita500",
	"Features": "Oven Type",
	"Display_value": "STATICO",
	"Original_value": "oven_type_STATICO"
},
{
	"Phase": "Efficienza5",
	"Features": "Oven Type",
	"Display_value": "STATICO",
	"Original_value": "oven_type_STATICO"
},
{
	"Phase": "Pressure80",
	"Features": "Oven Type",
	"Display_value": "STATICO",
	"Original_value": "oven_type_STATICO"
},
{
	"Phase": "Assessmento1",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "My Green",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "FADE1",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Autostrada",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Pressure40",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Efficienza2",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Efficienza6",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Efficienza1",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Efficienza5",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Pressure80",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Efficienza3",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Pressure100",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Pressure120",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Pressure500",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Characteristic Check",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Salita500",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Efficienza4",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Pressure180-200",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "FADE2",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Assessmento2",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Pressure160",
	"Features": "Thickness",
	"Display_value": "Numeric",
	"Original_value": "Thickness"
},
{
	"Phase": "Pressure160",
	"Features": "Anti Noise",
	"Display_value": "Binary",
	"Original_value": "anti_noise"
},
{
	"Phase": "Pressure500",
	"Features": "Anti Noise",
	"Display_value": "Binary",
	"Original_value": "anti_noise"
},
{
	"Phase": "Efficienza5",
	"Features": "Oven Cycle",
	"Display_value": "None"
},
{
	"Phase": "Efficienza2",
	"Features": "Oven Cycle",
	"Display_value": "None"
},
{
	"Phase": "Pressure180-200",
	"Features": "Oven Cycle",
	"Display_value": "None"
},
{
	"Phase": "Efficienza3",
	"Features": "Oven Cycle",
	"Display_value": "None"
},
{
	"Phase": "Efficienza4",
	"Features": "Oven Cycle",
	"Display_value": "None"
},
{
	"Phase": "Efficienza6",
	"Features": "Oven Cycle",
	"Display_value": "None"
},
{
	"Phase": "Efficienza5",
	"Features": "Oven Cycle",
	"Display_value": "CT2",
	"Original_value": "oven_cycle_CT2"
},
{
	"Phase": "Pressure180-200",
	"Features": "Oven Cycle",
	"Display_value": "IBR",
	"Original_value": "oven_cycle_IBR"
},
{
	"Phase": "Efficienza2",
	"Features": "Oven Cycle",
	"Display_value": "iG100",
	"Original_value": "oven_cycle_iG00"
},
{
	"Phase": "Efficienza4",
	"Features": "Oven Cycle",
	"Display_value": "iG100",
	"Original_value": "oven_cycle_iG00"
},
{
	"Phase": "Pressure180-200",
	"Features": "Oven Cycle",
	"Display_value": "iG00",
	"Original_value": "oven_cycle_iG00"
},
{
	"Phase": "Efficienza3",
	"Features": "Oven Cycle",
	"Display_value": "iH01",
	"Original_value": "oven_cycle_iH01"
},
{
	"Phase": "Efficienza4",
	"Features": "Oven Cycle",
	"Display_value": "iQ01",
	"Original_value": "oven_cycle_iQ01"
},
{
	"Phase": "Efficienza3",
	"Features": "Oven Cycle",
	"Display_value": "iQ01",
	"Original_value": "oven_cycle_iQ01"
},
{
	"Phase": "Pressure180-200",
	"Features": "Oven Cycle",
	"Display_value": "iQ01",
	"Original_value": "oven_cycle_iQ01"
},
{
	"Phase": "Efficienza6",
	"Features": "Oven Cycle",
	"Display_value": "CTV90",
	"Original_value": "oven_cycle_CTV90"
},
{
	"Phase": "Pressure500",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Characteristic Check",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure180-200",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza6",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "FADE1",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza2",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Assessmento1",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure100",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza4",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Assessmento2",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Autostrada",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza1",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure120",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza3",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "My Green",
	"Features": "Rotor Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza3",
	"Features": "Rotor Code",
	"Display_value": "TJ",
	"Original_value": "rotor_code_TJ"
},
{
	"Phase": "Efficienza3",
	"Features": "Rotor Code",
	"Display_value": "ZJ",
	"Original_value": "rotor_code_ZJ"
},
{
	"Phase": "Efficienza6",
	"Features": "Rotor Code",
	"Display_value": "ZJ",
	"Original_value": "rotor_code_ZJ"
},
{
	"Phase": "Efficienza4",
	"Features": "Rotor Code",
	"Display_value": "ABS",
	"Original_value": "rotor_code_ABS"
},
{
	"Phase": "FADE1",
	"Features": "Rotor Code",
	"Display_value": "AEJ",
	"Original_value": "rotor_code_AEJ"
},
{
	"Phase": "Assessmento2",
	"Features": "Rotor Code",
	"Display_value": "AKC",
	"Original_value": "rotor_code_AKC"
},
{
	"Phase": "Efficienza2",
	"Features": "Rotor Code",
	"Display_value": "AKC",
	"Original_value": "rotor_code_AKC"
},
{
	"Phase": "Pressure180-200",
	"Features": "Rotor Code",
	"Display_value": "ALH",
	"Original_value": "rotor_code_ALH"
},
{
	"Phase": "Efficienza6",
	"Features": "Rotor Code",
	"Display_value": "ARQ",
	"Original_value": "rotor_code_ARQ"
},
{
	"Phase": "Pressure180-200",
	"Features": "Rotor Code",
	"Display_value": "ATP",
	"Original_value": "rotor_code_ATP"
},
{
	"Phase": "Characteristic Check",
	"Features": "Rotor Code",
	"Display_value": "BAR",
	"Original_value": "rotor_code_BAR"
},
{
	"Phase": "My Green",
	"Features": "Rotor Code",
	"Display_value": "BAR",
	"Original_value": "rotor_code_BAR"
},
{
	"Phase": "Efficienza4",
	"Features": "Rotor Code",
	"Display_value": "BBT",
	"Original_value": "rotor_code_BBT"
},
{
	"Phase": "Assessmento1",
	"Features": "Rotor Code",
	"Display_value": "BEH",
	"Original_value": "rotor_code_BEH"
},
{
	"Phase": "Efficienza4",
	"Features": "Rotor Code",
	"Display_value": "BJN",
	"Original_value": "rotor_code_BJN"
},
{
	"Phase": "My Green",
	"Features": "Rotor Code",
	"Display_value": "R_Others_1",
	"Original_value": "rotor_code_R_Others_1"
},
{
	"Phase": "Efficienza2",
	"Features": "Rotor Code",
	"Display_value": "R_Others_1",
	"Original_value": "rotor_code_R_Others_1"
},
{
	"Phase": "Efficienza3",
	"Features": "Rotor Code",
	"Display_value": "R_Others_1",
	"Original_value": "rotor_code_R_Others_1"
},
{
	"Phase": "Assessmento2",
	"Features": "Rotor Code",
	"Display_value": "R_Others_1",
	"Original_value": "rotor_code_R_Others_1"
},
{
	"Phase": "Characteristic Check",
	"Features": "Rotor Code",
	"Display_value": "R_Others_1",
	"Original_value": "rotor_code_R_Others_1"
},
{
	"Phase": "Autostrada",
	"Features": "Rotor Code",
	"Display_value": "R_Others_2",
	"Original_value": "rotor_code_R_Others_2"
},
{
	"Phase": "Pressure120",
	"Features": "Rotor Code",
	"Display_value": "R_Others_2",
	"Original_value": "rotor_code_R_Others_2"
},
{
	"Phase": "Pressure100",
	"Features": "Rotor Code",
	"Display_value": "R_Others_2",
	"Original_value": "rotor_code_R_Others_2"
},
{
	"Phase": "Pressure500",
	"Features": "Rotor Code",
	"Display_value": "R_Others_3",
	"Original_value": "rotor_code_R_Others_3"
},
{
	"Phase": "Efficienza1",
	"Features": "Rotor Code",
	"Display_value": "R_Others_3",
	"Original_value": "rotor_code_R_Others_3"
},
{
	"Phase": "Pressure120",
	"Features": "Rotor Code",
	"Display_value": "R_Others_3",
	"Original_value": "rotor_code_R_Others_3"
},
{
	"Phase": "Characteristic Check",
	"Features": "Rotor Code",
	"Display_value": "R_Others_3",
	"Original_value": "rotor_code_R_Others_3"
},
{
	"Phase": "Assessmento2",
	"Features": "Rotor Code",
	"Display_value": "R_Others_4",
	"Original_value": "rotor_code_R_Others_4"
},
{
	"Phase": "Salita500",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Efficienza3",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Pressure500",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Autostrada",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Pressure40",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Efficienza6",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Pressure180-200",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Assessmento1",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Pressure80",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Pressure120",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Characteristic Check",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "FADE2",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Assessmento2",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Efficienza1",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "My Green",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Efficienza4",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Pressure160",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Pressure100",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Efficienza2",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Efficienza5",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "FADE1",
	"Features": "Rotor Size",
	"Display_value": "Numeric",
	"Original_value": "Rotor_Size"
},
{
	"Phase": "Autostrada",
	"Features": "Anti Noise ",
	"Display_value": "Binary",
	"Original_value": "anti_noise"
},
{
	"Phase": "Pressure100",
	"Features": "Anti Noise ",
	"Display_value": "Binary",
	"Original_value": "anti_noise"
},
{
	"Phase": "FADE2",
	"Features": "Anti Noise ",
	"Display_value": "Binary",
	"Original_value": "anti_noise"
},
{
	"Phase": "Pressure120",
	"Features": "Anti Noise ",
	"Display_value": "Numeric",
	"Original_value": "anti_noise"
},
{
	"Phase": "Efficienza1",
	"Features": "Anti Noise ",
	"Display_value": "Binary",
	"Original_value": "anti_noise"
},
{
	"Phase": "Pressure40",
	"Features": "Anti Noise ",
	"Display_value": "Binary",
	"Original_value": "anti_noise"
},
{
	"Phase": "Pressure80",
	"Features": "Anti Noise ",
	"Display_value": "Binary",
	"Original_value": "anti_noise"
},
{
	"Phase": "Characteristic Check",
	"Features": "Temperature",
	"Display_value": "Numeric",
	"Original_value": "Temperature"
},
{
	"Phase": "My Green",
	"Features": "Temperature",
	"Display_value": "Numeric",
	"Original_value": "Temperature"
},
{
	"Phase": "FADE2",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza5",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Salita500",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure120",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure160",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Assessmento1",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza1",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure100",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Assessmento2",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Autostrada",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure80",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "FADE1",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "My Green",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Characteristic Check",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza6",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza2",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure180-200",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure500",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Efficienza4",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Pressure40",
	"Features": "Caliper Code",
	"Display_value": "None"
},
{
	"Phase": "Assessmento1",
	"Features": "Caliper Code",
	"Display_value": "IO",
	"Original_value": "caliper_code_IO"
},
{
	"Phase": "Pressure500",
	"Features": "Caliper Code",
	"Display_value": "IO",
	"Original_value": "caliper_code_IO"
},
{
	"Phase": "Salita500",
	"Features": "Caliper Code",
	"Display_value": "IO",
	"Original_value": "caliper_code_IO"
},
{
	"Phase": "Efficienza2",
	"Features": "Caliper Code",
	"Display_value": "TJ",
	"Original_value": "caliper_code_TJ"
},
{
	"Phase": "FADE1",
	"Features": "Caliper Code",
	"Display_value": "UI",
	"Original_value": "caliper_code_UI"
},
{
	"Phase": "Assessmento2",
	"Features": "Caliper Code",
	"Display_value": "VV",
	"Original_value": "caliper_code_VV"
},
{
	"Phase": "Pressure180-200",
	"Features": "Caliper Code",
	"Display_value": "VV",
	"Original_value": "caliper_code_VV"
},
{
	"Phase": "FADE1",
	"Features": "Caliper Code",
	"Display_value": "ADE",
	"Original_value": "caliper_code_ADE"
},
{
	"Phase": "Assessmento2",
	"Features": "Caliper Code",
	"Display_value": "ALF",
	"Original_value": "caliper_code_ALF"
},
{
	"Phase": "Efficienza4",
	"Features": "Caliper Code",
	"Display_value": "ALF",
	"Original_value": "caliper_code_ALF"
},
{
	"Phase": "Efficienza4",
	"Features": "Caliper Code",
	"Display_value": "ALH",
	"Original_value": "caliper_code_ALH"
},
{
	"Phase": "Salita500",
	"Features": "Caliper Code",
	"Display_value": "APJ",
	"Original_value": "caliper_code_APJ"
},
{
	"Phase": "Efficienza2",
	"Features": "Caliper Code",
	"Display_value": "ARA",
	"Original_value": "caliper_code_ARA"
},
{
	"Phase": "Efficienza6",
	"Features": "Caliper Code",
	"Display_value": "ARQ",
	"Original_value": "caliper_code_ARQ"
},
{
	"Phase": "Salita500",
	"Features": "Caliper Code",
	"Display_value": "BEH",
	"Original_value": "caliper_code_BEH"
},
{
	"Phase": "Efficienza5",
	"Features": "Caliper Code",
	"Display_value": "BEH",
	"Original_value": "caliper_code_BEH"
},
{
	"Phase": "Assessmento1",
	"Features": "Caliper Code",
	"Display_value": "BEH",
	"Original_value": "caliper_code_BEH"
},
{
	"Phase": "Pressure120",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Efficienza1",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Pressure160",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Pressure80",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Characteristic Check",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Autostrada",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Pressure40",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Salita500",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "FADE2",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "My Green",
	"Features": "Caliper Code",
	"Display_value": "Numeric",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Pressure100",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Pressure500",
	"Features": "Caliper Code",
	"Display_value": "Other",
	"Original_value": "caliper_code_other_caliper_code"
},
{
	"Phase": "Efficienza4",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Pressure120",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Efficienza6",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Assessmento2",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "FADE2",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Pressure160",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Pressure180-200",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Salita500",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Autostrada",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Efficienza2",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "FADE1",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Efficienza5",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Pressure80",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Pressure40",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Pressure100",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "My Green",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Assessmento1",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Efficienza3",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Efficienza1",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Pressure500",
	"Features": "Pistion Area",
	"Display_value": "Numeric",
	"Original_value": "pistion_area"
},
{
	"Phase": "Efficienza5",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Pressure160",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Assessmento2",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Pressure120",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Pressure500",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Pressure180-200",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Pressure100",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "My Green",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "FADE1",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Efficienza3",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Pressure40",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Autostrada",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Salita500",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Efficienza6",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "FADE2",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Efficienza2",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Assessmento1",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Pressure80",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Characteristic Check",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Efficienza1",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Efficienza4",
	"Features": "Maximum Speed",
	"Display_value": "Numeric",
	"Original_value": "max_speed"
},
{
	"Phase": "Pressure160",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "My Green",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "Pressure500",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "Efficienza1",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "Pressure120",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "Pressure100",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "Efficienza3",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "Pressure80",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "Pressure40",
	"Features": "Press Machine",
	"Display_value": "None"
},
{
	"Phase": "Pressure500",
	"Features": "Press Machine",
	"Display_value": "30 T",
	"Original_value": "press_machine_30T"
},
{
	"Phase": "Pressure80",
	"Features": "Press Machine",
	"Display_value": "30 T",
	"Original_value": "press_machine_30T"
},
{
	"Phase": "Pressure100",
	"Features": "Press Machine",
	"Display_value": "60 T",
	"Original_value": "press_machine_60T"
},
{
	"Phase": "Efficienza3",
	"Features": "Press Machine",
	"Display_value": "60 T",
	"Original_value": "press_machine_60T"
},
{
	"Phase": "Efficienza1",
	"Features": "Press Machine",
	"Display_value": "60 T",
	"Original_value": "press_machine_60T"
},
{
	"Phase": "Pressure40",
	"Features": "Press Machine",
	"Display_value": "60 T",
	"Original_value": "press_machine_60T"
},
{
	"Phase": "Pressure160",
	"Features": "Press Machine",
	"Display_value": "60T",
	"Original_value": "press_machine_60T"
},
{
	"Phase": "Pressure120",
	"Features": "Press Machine",
	"Display_value": "60 T",
	"Original_value": "press_machine_60T"
},
{
	"Phase": "My Green",
	"Features": "Press Machine",
	"Display_value": "Other",
	"Original_value": "press_machine_other_press_machine"
},
{
	"Phase": "Pressure180-200",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Autostrada",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "My Green",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Efficienza6",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Characteristic Check",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Assessmento1",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Pressure160",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Assessmento2",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Efficienza2",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "FADE1",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Efficienza1",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "FADE2",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Efficienza5",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Salita500",
	"Features": "Braking System",
	"Display_value": "None"
},
{
	"Phase": "Efficienza6",
	"Features": "Braking System",
	"Display_value": "DC ",
	"Original_value": "braking_system_DC"
},
{
	"Phase": "Autostrada",
	"Features": "Braking System",
	"Display_value": "DC",
	"Original_value": "braking_system_DC"
},
{
	"Phase": "Efficienza5",
	"Features": "Braking System",
	"Display_value": "DC",
	"Original_value": "braking_system_DC"
},
{
	"Phase": "FADE2",
	"Features": "Braking System",
	"Display_value": "DC",
	"Original_value": "braking_system_DC"
},
{
	"Phase": "FADE2",
	"Features": "Braking System",
	"Display_value": "BMW",
	"Original_value": "braking_system_BMW"
},
{
	"Phase": "FADE1",
	"Features": "Braking System",
	"Display_value": "BMW",
	"Original_value": "braking_system_BMW"
},
{
	"Phase": "FADE1",
	"Features": "Braking System",
	"Display_value": "CHR",
	"Original_value": "braking_system_CHR"
},
{
	"Phase": "Assessmento1",
	"Features": "Braking System",
	"Display_value": "CHR",
	"Original_value": "braking_system_CHR"
},
{
	"Phase": "Efficienza2",
	"Features": "Braking System",
	"Display_value": "CHR",
	"Original_value": "braking_system_CHR"
},
{
	"Phase": "Salita500",
	"Features": "Braking System",
	"Display_value": "DCM",
	"Original_value": "braking_system_DCM"
},
{
	"Phase": "My Green",
	"Features": "Braking System",
	"Display_value": "DCM",
	"Original_value": "braking_system_DCM"
},
{
	"Phase": "Assessmento2",
	"Features": "Braking System",
	"Display_value": "RDB",
	"Original_value": "braking_system_RDB"
},
{
	"Phase": "Assessmento1",
	"Features": "Braking System",
	"Display_value": "RDB",
	"Original_value": "braking_system_RDB"
},
{
	"Phase": "FADE1",
	"Features": "Braking System",
	"Display_value": "RDB",
	"Original_value": "braking_system_RDB"
},
{
	"Phase": "Salita500",
	"Features": "Braking System",
	"Display_value": "RDB",
	"Original_value": "braking_system_RDB"
},
{
	"Phase": "Characteristic Check",
	"Features": "Braking System",
	"Display_value": "REN",
	"Original_value": "braking_system_REN"
},
{
	"Phase": "Autostrada",
	"Features": "Braking System",
	"Display_value": "VWN",
	"Original_value": "braking_system_VWN"
},
{
	"Phase": "FADE1",
	"Features": "Braking System",
	"Display_value": "VWN",
	"Original_value": "braking_system_VWN"
},
{
	"Phase": "Pressure160",
	"Features": "Braking System",
	"Display_value": "VWN",
	"Original_value": "braking_system_VWN"
},
{
	"Phase": "FADE2",
	"Features": "Braking System",
	"Display_value": "VWN",
	"Original_value": "braking_system_VWN"
},
{
	"Phase": "Pressure180-200",
	"Features": "Braking System",
	"Display_value": "FIAT",
	"Original_value": "braking_system_FIAT"
},
{
	"Phase": "Efficienza2",
	"Features": "Braking System",
	"Display_value": "FORD",
	"Original_value": "braking_system_FORD"
},
{
	"Phase": "FADE1",
	"Features": "Braking System",
	"Display_value": "FORD",
	"Original_value": "braking_system_FORD"
},
{
	"Phase": "Assessmento2",
	"Features": "Braking System",
	"Display_value": "FORD",
	"Original_value": "braking_system_FORD"
},
{
	"Phase": "FADE1",
	"Features": "Braking System",
	"Display_value": "PORSCHE",
	"Original_value": "braking_system_PORSCHE "
},
{
	"Phase": "My Green",
	"Features": "Braking System",
	"Display_value": "Numeric",
	"Original_value": "braking_system_PEU \u0026 PEUG"
},
{
	"Phase": "Assessmento2",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Efficienza5",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Assessmento1",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Autostrada",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Pressure80",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "FADE1",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Efficienza2",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "FADE2",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Efficienza4",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "My Green",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Efficienza6",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Pressure500",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Pressure160",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Pressure100",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Efficienza1",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Pressure180-200",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Pressure120",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Efficienza3",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Pressure40",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Salita500",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Characteristic Check",
	"Features": "Press Pressure",
	"Display_value": "Numeric",
	"Original_value": "press_pressure"
},
{
	"Phase": "Pressure120",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Pressure100",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Efficienza2",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Salita500",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Autostrada",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Efficienza3",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Pressure160",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Pressure80",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Efficienza4",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Pressure180-200",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Assessmento1",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Pressure40",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "FADE1",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Assessmento2",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "My Green",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Pressure500",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "FADE2",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Characteristic Check",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Efficienza1",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Efficienza5",
	"Features": "Rotor Condition",
	"Display_value": "Numeric",
	"Original_value": "rotor_condition"
},
{
	"Phase": "Pressure80",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Efficienza3",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Salita500",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Efficienza1",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Pressure100",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Efficienza2",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Pressure40",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Autostrada",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Pressure120",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "FADE2",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Efficienza4",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Pressure180-200",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Assessmento1",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Efficienza6",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Efficienza5",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "FADE1",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Assessmento2",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Pressure160",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Pressure500",
	"Features": "Mean Temperature",
	"Display_value": "Numeric",
	"Original_value": "Mean_temp"
},
{
	"Phase": "Pressure40",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Pressure160",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "My Green",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Assessmento1",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Pressure120",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Salita500",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Efficienza2",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Autostrada",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Pressure500",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Pressure80",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Efficienza6",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Assessmento2",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Pressure100",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Efficienza3",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Pressure180-200",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "FADE2",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Efficienza4",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "FADE1",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Efficienza1",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Characteristic Check",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Efficienza5",
	"Features": "Aggregate Density",
	"Display_value": "Numeric",
	"Original_value": "Agg_Density"
},
{
	"Phase": "Pressure120",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Efficienza2",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Autostrada",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Efficienza1",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Efficienza6",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Salita500",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Pressure160",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Pressure100",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "FADE2",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "My Green",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Efficienza3",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Pressure40",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Assessmento1",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Pressure80",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Efficienza5",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Pressure500",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Characteristic Check",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Pressure180-200",
	"Features": "Minimum Oven Time",
	"Display_value": "Numeric",
	"Original_value": "oven_time_min"
},
{
	"Phase": "Pressure40",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Efficienza4",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Efficienza5",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Assessmento1",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "FADE2",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Efficienza2",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Pressure120",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Autostrada",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Pressure100",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Efficienza1",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Pressure160",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Salita500",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Efficienza6",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "FADE1",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Assessmento2",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Pressure180-200",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Pressure500",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Pressure80",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Efficienza3",
	"Features": "Press Plate Temperature",
	"Display_value": "Numeric",
	"Original_value": "press_plate_temp"
},
{
	"Phase": "Characteristic Check",
	"Features": "Maximum Oven Temperature",
	"Display_value": "Numeric",
	"Original_value": "oven_temperature_max"
}
];
var dropDownList = ["None","CHR", "RDB", "IO", "BEH", "IR", "Binary", "ALF", "AKC", "VV", "FORD", "R_Others_1", "R_Others_4", "Other", "DC", "VWN", "R_Others_2", "STATICO", "REN", "BAR", "R_Others_3", "60 T", "ARA", "iG100", "TJ", "ZJ", "iQ01", "iH01", "BBT", "ALH", "BJN", "ABS", "CT2", "CTV90", "ARQ", "DC ", "AEJ", "BMW", "ADE", "UI", "PORSCHE", "DCM", "30 T", "60T", "ATP", "IBR", "FIAT", "iG00", "APJ"];



var phaseFeaturesData = {};
var phaseMapping = {};
$(document).ready(function() {
	console.log( "document loaded" );
});

$(window).on('load', function() {
	console.log( "window loaded" );
	//data to object
	//console.log(uiData)
	phaseFeaturesData = {};
	uiData.forEach(function(element) {
    //console.log(element);


    if(!phaseFeaturesData[element.Phase]){
    	phaseFeaturesData[element.Phase] = [];
    }

    if(phaseFeaturesData[element.Phase].indexOf(element.Features) == -1){
    	phaseFeaturesData[element.Phase].push(element.Features)
    }

 //    if(dropDownList.indexOf(element.Display_value) == -1){
 //    	phaseMapping[element.Phase] = {
 //    		Feature : element.Features,
 //    		type: 'textbox',
 //    		Display_value :element.Display_value,
 //    		Original_value: element.Original_value

 //    	}
 //    }else{
 //    	if(element.Display_value != "None")
 //    		if(phaseMapping[element.Phase]['Display_value']){
 //    			console.log(element,phaseMapping)
	// 			phaseMapping[element.Phase]['Display_value'].push(element.Display_value)
	// 			phaseMapping[element.Phase]['Original_value'].push(element.Original_value)
	// 		}else{
 //    			phaseMapping[element.Phase] = {
 //    				Feature : element.Features,
 //    				type: 'dropDown',
 //    				Display_value:[],
 //    				Original_value:[]
	// 		    };
	// 		    phaseMapping[element.Phase]['Display_value'].push(element.Display_value)
	// 		    phaseMapping[element.Phase]['Original_value'].push(element.Original_value)
	// 		}

	// }
	if(element.Display_value == "Numeric"){
		phaseMapping[element.Phase+"_"+element.Features] = {
			Feature : element.Features,
			type: 'textbox',
			Display_value :element.Display_value,
			Original_value: element.Original_value

		}
	}else{
        if(element.Display_value == "Binary"){
            phaseMapping[element.Phase+"_"+element.Features] = {
            Feature : element.Features,
			type: 'binary',
			Display_value :element.Display_value,
			Original_value: element.Original_value
            }
        }else{
            
        
		if(element.Display_value != "None" ){
			if(phaseMapping[element.Phase+"_"+element.Features] && phaseMapping[element.Phase+"_"+element.Features]['Display_value']){
				phaseMapping[element.Phase+"_"+element.Features]['Display_value'].push(element.Display_value);
				phaseMapping[element.Phase+"_"+element.Features]['Original_value'].push(element.Original_value);
			}else{
				phaseMapping[element.Phase+"_"+element.Features] = {
					Feature : element.Features,
					type: 'dropDown',
					Display_value :[],
					Original_value: []

				}
				phaseMapping[element.Phase+"_"+element.Features]['Display_value'].push(element.Display_value);
				phaseMapping[element.Phase+"_"+element.Features]['Original_value'].push(element.Original_value);

			}
		}	
    }
	}

});

	console.log(phaseMapping)
	console.log(phaseFeaturesData)
    
    var phaseSelectedUrl = getUrlVars()["phaseSelector"];
    console.log(phaseSelectedUrl)
    phaseSelectedUrl = unescape(phaseSelectedUrl)
    console.log(phaseSelectedUrl)
    if(phaseSelectedUrl != undefined || phaseSelectedUrl != ''){
        displayDynamicForm(phaseSelectedUrl)
        $( "#phaseSelector" ).val(phaseSelectedUrl)
    }
    console.log('OnLoad',phaseSelectedUrl)
    
});

function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function displayDynamicForm(selectedPhase){
        var row ='';

            var columsData ='';
        $('#dynamicForm').empty()
    for(var i=0;i<phaseFeaturesData[selectedPhase].length;i++){
        var key = selectedPhase+"_"+phaseFeaturesData[selectedPhase][i];
        console.log(i,key)
        var formField = '';

        if(phaseMapping[key]['type'] == "textbox"){
            columsData += '<div class="col-md-4"><label for="'+phaseMapping[key]['Original_value']+'">'+phaseMapping[key]['Feature']+':</label><input type="text" class="form-control" id="'+phaseMapping[key]['Original_value']+'" name="'+phaseMapping[key]['Original_value']+'"></div>'; 
        }else{
            if(phaseMapping[key]['type'] == "binary"){
                 columsData += '<div class="col-md-4"><label for="'+phaseMapping[key]['Feature']+'"> '+phaseMapping[key]['Feature']+':</label><select class="form-control" id="'+phaseMapping[key]['Feature']+'" name="'+phaseMapping[key]['Feature']+'"><option value="True">True</option><option value="False">False</option></select></div>';
                
            }else{
                 console.log('dropDown')
            //var opt = '<div class="col-sm-6">';
            var	label = '';
            var opt = '';
            label = '<div class="col-md-4"><label for="'+phaseMapping[key]['Feature']+'"> '+phaseMapping[key]['Feature']+':</label><select class="form-control" id="'+phaseMapping[key]['Feature']+'" name="'+phaseMapping[key]['Feature']+'"><option value="None">None of This</option>';
            for(var j=0;j<phaseMapping[key]['Original_value'].length;j++){
                opt += '<option value="'+phaseMapping[key]['Original_value'][j]+'">'+phaseMapping[key]['Display_value'][j]+'</option>';
            }
            columsData += label+opt+'</select></div>';
            console.log('Hello',i,columsData);
            }
           
//            $('#dynamicForm').append(label+opt);
           // opt = '';
        }
        if((i+1) % 3 == 0 || i == phaseFeaturesData[selectedPhase].length-1){
            row = '<div class="row">'+columsData+'</div>';
            columsData = '';
            opt= '';
            console.log('APPEND',i,columsData);
            $('#dynamicForm').append(row)
        }

    }
}
$('#phaseSelector').change(function(e) {
    var selectedPhase = $( "#phaseSelector" ).val() ;
    displayDynamicForm(selectedPhase);
});


// $( "form" ).on( "submit", function( event ) {
//   event.preventDefault();
//     var data = JSON.stringify($( this ).serialize() );
//     console.log(data)
    
// //  console.log(JSON.stringify($( this ).serialize() ));
// });
        

  $('form').submit(function(event){
  	  event.preventDefault();
		console.log('Inside This')
    $.ajax({
      url: 'http://35.202.234.203/api',
      type: 'GET',
      data : $('form').serialize(),
      success: function(data){
      	console.log(data);
      	data = JSON.parse(data)
        $('.outputTable').html('<div class="ans"><p> Class: '+data.class+'</p><p>Confidence: '+data.confidence+'%</p></div>')
      }
    });
})                         

//$("#dynamicForm").append(formField)
//$("#dynamicForm").append('<div class="form-group"><label for="usr">Name:</label><input type="text" class="form-control" id="usr"></div>')
//var Feature = phaseMapping[key]['Feature']
	//console.log(phaseMapping[key]); 
//    
//    <div class="row"></div>
//    
//      <div class="row">
//                <div class="col-md-4"><label for=${phaseMapping[key]['Original_value']}>${phaseMapping[key]['Feature']}: </label><input type="text" class="form-control" id=${phaseMapping[key]['Original_value']} name=${phaseMapping[key]['Original_value']}></div>
//                
//                <div class="col-md-4"><label for=${phaseMapping[key]['Original_value']}>${phaseMapping[key]['Feature']}: </label><input type="text" class="form-control" id=${phaseMapping[key]['Original_value']} name=${phaseMapping[key]['Original_value']}></div>
//                
//                <div class="col-sm-4">
//                    <label for='${phaseMapping[key]['Feature']}'> ${phaseMapping[key]['Feature']} *:</label>
//                     <select class="form-control" id="${phaseMapping[key]['Feature']}" name="${phaseMapping[key]['Feature']}">
//                           <option value="${phaseMapping[key]['Original_value'][j]}">${phaseMapping[key]['Display_value'][j]}</option>
//                         
//                    </select>
//                </div>
//        </div>                            
//                   
//    




